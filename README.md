# BCware Flow NFT App Testcases


## NonFungibleToken contract interface

The interface that all non-fungible token contracts could conform to.

## NftItems smart contract
The resource that stores a user's NFT collection.
It includes a few functions to allow the owner to easily move tokens in and out of the collection.

## Install flow-cli
1. Install CLI using below command  
Linux  
`
sh -ci "$(curl -fsSL https://storage.googleapis.com/flow-cli/install.sh)"
`  
Windows  
`
iex "& { $(irm 'https://storage.googleapis.com/flow-cli/install.ps1') }"
`
  

## NFT-ITEMS Cadence Contracts and Test Cases (Using Emulator)
2. Clone the code from gitlab repo using a different console  
`
git clone https://gitlab.com/kiran.kumarhm/flowcdcreview.git
`

3.	Install node modules  
`
	cd flowcdcreview/test_cases/cadence/tests  
`  
`
	npm install
`  
4.	Run test cases  
`
npm run test
`
  

The results of the test scripts ran using the emulator are below :
  

![Project Overview](test_cases/cadence/tests/TestCaseResults.png)
  

Note: Same contracts we have deployed in testnet, here our testnet contract address 
“0xa4df2ca3e19f32ef”.
