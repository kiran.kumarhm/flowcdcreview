package test

import (
	"strings"
	"testing"

	"github.com/onflow/cadence"
	jsoncdc "github.com/onflow/cadence/encoding/json"
	emulator "github.com/onflow/flow-emulator"
	sdk "github.com/onflow/flow-go-sdk"
	"github.com/onflow/flow-go-sdk/crypto"
	sdktemplates "github.com/onflow/flow-go-sdk/templates"
	"github.com/onflow/flow-go-sdk/test"

	"github.com/stretchr/testify/assert"

	"github.com/onflow/flow-go-sdk"

	nft_contracts "github.com/onflow/flow-nft/lib/go/contracts"
)

const (
	NftItemsRootPath                   = "../../../../cadence"
	NftItemsNftItemsPath             = NftItemsRootPath + "/contracts/NftItems.cdc"
	NftItemsSetupAccountPath           = NftItemsRootPath + "/transactions/NftItems/setup_account.cdc"
	NftItemsMintNftItemPath          = NftItemsRootPath + "/transactions/NftItems/mint_Nft_item.cdc"
	NftItemsTransferNftItemPath      = NftItemsRootPath + "/transactions/NftItems/transfer_Nft_item.cdc"
	NftItemsInspectNftItemSupplyPath = NftItemsRootPath + "/scripts/NftItems/read_Nft_items_supply.cdc"
	NftItemsInspectCollectionLenPath   = NftItemsRootPath + "/scripts/NftItems/read_collection_length.cdc"
	NftItemsInspectCollectionIdsPath   = NftItemsRootPath + "/scripts/NftItems/read_collection_ids.cdc"

	typeID1 = 1000
	typeID2 = 2000
)

func NftItemsDeployContracts(b *emulator.Blockchain, t *testing.T) (flow.Address, flow.Address, crypto.Signer) {
	accountKeys := test.AccountKeyGenerator()

	// Should be able to deploy a contract as a new account with no keys.
	nftCode := loadNonFungibleToken()
	nftAddr, err := b.CreateAccount(
		nil,
		[]sdktemplates.Contract{
			{
				Name:   "NonFungibleToken",
				Source: string(nftCode),
			},
		})
	if !assert.NoError(t, err) {
		t.Log(err.Error())
	}
	_, err = b.CommitBlock()
	assert.NoError(t, err)

	// Should be able to deploy a contract as a new account with one key.
	NftItemsAccountKey, NftItemsSigner := accountKeys.NewWithSigner()
	NftItemsCode := loadNftItems(nftAddr.String())
	NftItemsAddr, err := b.CreateAccount(
		[]*flow.AccountKey{NftItemsAccountKey},
		[]sdktemplates.Contract{
			{
				Name:   "NftItems",
				Source: string(NftItemsCode),
			},
		})
	if !assert.NoError(t, err) {
		t.Log(err.Error())
	}
	_, err = b.CommitBlock()
	assert.NoError(t, err)

	// Simplify the workflow by having the contract address also be our initial test collection.
	NftItemsSetupAccount(t, b, NftItemsAddr, NftItemsSigner, nftAddr, NftItemsAddr)

	return nftAddr, NftItemsAddr, NftItemsSigner
}

func NftItemsSetupAccount(t *testing.T, b *emulator.Blockchain, userAddress sdk.Address, userSigner crypto.Signer, nftAddr sdk.Address, NftItemsAddr sdk.Address) {
	tx := flow.NewTransaction().
		SetScript(NftItemsGenerateSetupAccountScript(nftAddr.String(), NftItemsAddr.String())).
		SetGasLimit(100).
		SetProposalKey(b.ServiceKey().Address, b.ServiceKey().Index, b.ServiceKey().SequenceNumber).
		SetPayer(b.ServiceKey().Address).
		AddAuthorizer(userAddress)

	signAndSubmit(
		t, b, tx,
		[]flow.Address{b.ServiceKey().Address, userAddress},
		[]crypto.Signer{b.ServiceKey().Signer(), userSigner},
		false,
	)
}

func NftItemsCreateAccount(t *testing.T, b *emulator.Blockchain, nftAddr sdk.Address, NftItemsAddr sdk.Address) (sdk.Address, crypto.Signer) {
	userAddress, userSigner, _ := createAccount(t, b)
	NftItemsSetupAccount(t, b, userAddress, userSigner, nftAddr, NftItemsAddr)
	return userAddress, userSigner
}

func NftItemsMintItem(b *emulator.Blockchain, t *testing.T, nftAddr, NftItemsAddr flow.Address, NftItemsSigner crypto.Signer, typeID uint64) {
	tx := flow.NewTransaction().
		SetScript(NftItemsGenerateMintNftItemScript(nftAddr.String(), NftItemsAddr.String())).
		SetGasLimit(100).
		SetProposalKey(b.ServiceKey().Address, b.ServiceKey().Index, b.ServiceKey().SequenceNumber).
		SetPayer(b.ServiceKey().Address).
		AddAuthorizer(NftItemsAddr)
	tx.AddArgument(cadence.NewAddress(NftItemsAddr))
	tx.AddArgument(cadence.NewUInt64(typeID))

	signAndSubmit(
		t, b, tx,
		[]flow.Address{b.ServiceKey().Address, NftItemsAddr},
		[]crypto.Signer{b.ServiceKey().Signer(), NftItemsSigner},
		false,
	)
}

func NftItemsTransferItem(b *emulator.Blockchain, t *testing.T, nftAddr, NftItemsAddr flow.Address, NftItemsSigner crypto.Signer, typeID uint64, recipientAddr flow.Address, shouldFail bool) {
	tx := flow.NewTransaction().
		SetScript(NftItemsGenerateTransferNftItemScript(nftAddr.String(), NftItemsAddr.String())).
		SetGasLimit(100).
		SetProposalKey(b.ServiceKey().Address, b.ServiceKey().Index, b.ServiceKey().SequenceNumber).
		SetPayer(b.ServiceKey().Address).
		AddAuthorizer(NftItemsAddr)
	tx.AddArgument(cadence.NewAddress(recipientAddr))
	tx.AddArgument(cadence.NewUInt64(typeID))

	signAndSubmit(
		t, b, tx,
		[]flow.Address{b.ServiceKey().Address, NftItemsAddr},
		[]crypto.Signer{b.ServiceKey().Signer(), NftItemsSigner},
		shouldFail,
	)
}


func (t *ServiceTest) TestNftItemsDeployContracts() {
	b := newEmulator()
	NftItemsDeployContracts(b, t.Test)
}

func (t *ServiceTest) TestCreateNftItem() {
	b := newEmulator()

	nftAddr, NftItemsAddr, NftItemsSigner := NftItemsDeployContracts(b, t.Test)

	supply := executeScriptAndCheck(t.Test, b, NftItemsGenerateInspectNftItemSupplyScript(nftAddr.String(), NftItemsAddr.String()), nil)
	assert.Equal(t.Test, cadence.NewUInt64(0), supply.(cadence.UInt64))

	len := executeScriptAndCheck(
		t.Test,
		b,
		NftItemsGenerateInspectCollectionLenScript(nftAddr.String(), NftItemsAddr.String()),
		[][]byte{jsoncdc.MustEncode(cadence.NewAddress(NftItemsAddr))},
	)
	assert.Equal(t.Test, cadence.NewInt(0), len.(cadence.Int))

	t.Test.Run("Should be able to mint a NftItems", func(t *testing.T) {
		NftItemsMintItem(b, t, nftAddr, NftItemsAddr, NftItemsSigner, typeID1)

		// Assert that the account's collection is correct
		len := executeScriptAndCheck(
			t,
			b,
			NftItemsGenerateInspectCollectionLenScript(nftAddr.String(), NftItemsAddr.String()),
			[][]byte{jsoncdc.MustEncode(cadence.NewAddress(NftItemsAddr))},
		)
		assert.Equal(t, cadence.NewInt(1), len.(cadence.Int))

		// Assert that the token type is correct
		/*typeID := executeScriptAndCheck(
			t,
			b,
			NftItemsGenerateInspectNftItemTypeIDScript(nftAddr.String(), NftItemsAddr.String()),
			// Cheat: We know it's token ID 0
			[][]byte{jsoncdc.MustEncode(cadence.NewUInt64(0))},
		)
		assert.Equal(t, cadence.NewUInt64(typeID1), typeID.(cadence.UInt64))*/
	})

	/*t.Run("Shouldn't be able to borrow a reference to an NFT that doesn't exist", func(t *testing.T) {
		// Assert that the account's collection is correct
		result, err := b.ExecuteScript(NftItemsGenerateInspectCollectionScript(nftAddr, NftItemsAddr, NftItemsAddr, "NftItems", "NftItemsCollection", 5), nil)
		require.NoError(t, err)
		assert.True(t, result.Reverted())
	})*/
}

func (t *ServiceTest) TestTransferNFT() {
	b := newEmulator()

	nftAddr, NftItemsAddr, NftItemsSigner := NftItemsDeployContracts(b, t.Test)

	userAddress, userSigner, _ := createAccount(t.Test, b)

	// create a new Collection
	t.Test.Run("Should be able to create a new empty NFT Collection", func(t *testing.T) {
		NftItemsSetupAccount(t, b, userAddress, userSigner, nftAddr, NftItemsAddr)

		len := executeScriptAndCheck(
			t,
			b, NftItemsGenerateInspectCollectionLenScript(nftAddr.String(), NftItemsAddr.String()),
			[][]byte{jsoncdc.MustEncode(cadence.NewAddress(userAddress))},
		)
		assert.Equal(t, cadence.NewInt(0), len.(cadence.Int))

	})

	t.Test.Run("Shouldn't be able to withdraw an NFT that doesn't exist in a collection", func(t *testing.T) {
		NftItemsTransferItem(b, t, nftAddr, NftItemsAddr, NftItemsSigner, 3333333, userAddress, true)

		//executeScriptAndCheck(t, b, NftItemsGenerateInspectCollectionLenScript(nftAddr, NftItemsAddr, userAddress, "NftItems", "NftItemsCollection", 0))

		// Assert that the account's collection is correct
		//executeScriptAndCheck(t, b, NftItemsGenerateInspectCollectionLenScript(nftAddr, NftItemsAddr, NftItemsAddr, "NftItems", "NftItemsCollection", 1))
	})

	// transfer an NFT
	t.Test.Run("Should be able to withdraw an NFT and deposit to another accounts collection", func(t *testing.T) {
		NftItemsMintItem(b, t, nftAddr, NftItemsAddr, NftItemsSigner, typeID1)
		// Cheat: we have minted one item, its ID will be zero
		NftItemsTransferItem(b, t, nftAddr, NftItemsAddr, NftItemsSigner, 0, userAddress, false)

		// Assert that the account's collection is correct
		//executeScriptAndCheck(t, b, NftItemsGenerateInspectCollectionScript(nftAddr, NftItemsAddr, userAddress, "NftItems", "NftItemsCollection", 0))

		//executeScriptAndCheck(t, b, NftItemsGenerateInspectCollectionLenScript(nftAddr, NftItemsAddr, userAddress, "NftItems", "NftItemsCollection", 1))

		// Assert that the account's collection is correct
		//executeScriptAndCheck(t, b, NftItemsGenerateInspectCollectionLenScript(nftAddr, NftItemsAddr, NftItemsAddr, "NftItems", "NftItemsCollection", 0))
	})

	// transfer an NFT
	/*t.Run("Should be able to withdraw an NFT and destroy it, not reducing the supply", func(t *testing.T) {
		tx := flow.NewTransaction().
			SetScript(NftItemsGenerateDestroyScript(nftAddr, NftItemsAddr, "NftItems", "NftItemsCollection", 0)).
			SetGasLimit(100).
			SetProposalKey(b.ServiceKey().Address, b.ServiceKey().Index, b.ServiceKey().SequenceNumber).
			SetPayer(b.ServiceKey().Address).
			AddAuthorizer(userAddress)

		signAndSubmit(
			t, b, tx,
			[]flow.Address{b.ServiceKey().Address, userAddress},
			[]crypto.Signer{b.ServiceKey().Signer(), userSigner},
			false,
		)

		executeScriptAndCheck(t, b, NftItemsGenerateInspectCollectionLenScript(nftAddr, NftItemsAddr, userAddress, "NftItems", "NftItemsCollection", 0))

		// Assert that the account's collection is correct
		executeScriptAndCheck(t, b, NftItemsGenerateInspectCollectionLenScript(nftAddr, NftItemsAddr, NftItemsAddr, "NftItems", "NftItemsCollection", 0))

		executeScriptAndCheck(t, b, NftItemsGenerateInspectNFTSupplyScript(nftAddr, NftItemsAddr, "NftItems", 1))

	})*/
}

func replaceNftItemsAddressPlaceholders(code, nftAddress, NftItemsAddress string) []byte {
	return []byte(replaceStrings(
		code,
		map[string]string{
			nftAddressPlaceholder:        "0x" + nftAddress,
		},
	))
}

func loadNonFungibleToken() []byte {
	return nft_contracts.NonFungibleToken()
}

func loadNftItems(nftAddr string) []byte {
	return []byte(strings.ReplaceAll(
		string(readFile(NftItemsNftItemsPath)),
		nftAddressPlaceholder,
		"0x"+nftAddr,
	))
}

func NftItemsGenerateSetupAccountScript(nftAddr, NftItemsAddr string) []byte {
	return replaceNftItemsAddressPlaceholders(
		string(readFile(NftItemsSetupAccountPath)),
		nftAddr,
		NftItemsAddr,
	)
}

func NftItemsGenerateMintNftItemScript(nftAddr, NftItemsAddr string) []byte {
	return replaceNftItemsAddressPlaceholders(
		string(readFile(NftItemsMintNftItemPath)),
		nftAddr,
		NftItemsAddr,
	)
}

func NftItemsGenerateTransferNftItemScript(nftAddr, NftItemsAddr string) []byte {
	return replaceNftItemsAddressPlaceholders(
		string(readFile(NftItemsTransferNftItemPath)),
		nftAddr,
		NftItemsAddr,
	)
}

func NftItemsGenerateInspectNftItemSupplyScript(nftAddr, NftItemsAddr string) []byte {
	return replaceNftItemsAddressPlaceholders(
		string(readFile(NftItemsInspectNftItemSupplyPath)),
		nftAddr,
		NftItemsAddr,
	)
}

func NftItemsGenerateInspectCollectionLenScript(nftAddr, NftItemsAddr string) []byte {
	return replaceNftItemsAddressPlaceholders(
		string(readFile(NftItemsInspectCollectionLenPath)),
		nftAddr,
		NftItemsAddr,
	)
}

func NftItemsGenerateInspectCollectionIdsScript(nftAddr, NftItemsAddr string) []byte {
	return replaceNftItemsAddressPlaceholders(
		string(readFile(NftItemsInspectCollectionIdsPath)),
		nftAddr,
		NftItemsAddr,
	)
}

