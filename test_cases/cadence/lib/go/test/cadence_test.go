package test

import "testing"

type ServiceTest struct {
	Test *testing.T
}

func TestRunner(t *testing.T) {

	//t.Run("A=kibble1", func(t *testing.T) {
	//	test := ServiceTest{Test: t}
	//	test.TestKibbleDeployment()
	// })
	// t.Run("B=kibble2", func(t *testing.T) {
	// 	test := ServiceTest{Test: t}
	// 	test.TestKibbleSetupAccount()
	// })
	// t.Run("C=kibble3", func(t *testing.T) {
	// 	test := ServiceTest{Test: t}
	// 	test.TestKibbleMinting()
	// })
	// t.Run("D=kibble4", func(t *testing.T) {
	// 	test := ServiceTest{Test: t}
	// 	test.TestKibbleTransfers()
	// })
	//t.Run("E=NftItems1", func(t *testing.T) {
		//test := ServiceTest{Test: t}
		//test.TestNftItemsMarketDeployContracts()
	//})
	//t.Run("F=NftItems2", func(t *testing.T) {
	//  test := ServiceTest{Test: t}
    //  test.TestNftItemsMarketSetupAccount()
	//})
	//t.Run("G=NftItems3", func(t *testing.T) {
	// test := ServiceTest{Test: t}
	// test.TestNftItemsMarketCreateSaleOffer()
	//})
	t.Run("H=NftItemsMarketplace1", func(t *testing.T) {
		test := ServiceTest{Test: t}
		test.TestNftItemsDeployContracts()
	})
	t.Run("I=NftItemsMarketplace2", func(t *testing.T) {
		test := ServiceTest{Test: t}
		test.TestCreateNftItem()
	})
	t.Run("J=NftItemsMarketplace3", func(t *testing.T) {
		test := ServiceTest{Test: t}
		test.TestTransferNFT()
	})
}
