import path from "path";

import {
	emulator,
	init,
	getAccountAddress,
	shallPass,
	shallResolve,
	shallRevert,
} from "flow-js-testing";

import { getNftAdminAddress } from "../src/common";
import {
	deployNftItems,
	getNftItemCount,
	getNftItemSupply,
	mintNftItem,
	setupNftItemsOnAccount,
	transferNftItem,
} from "../src/nft-items";

const assetId = 1313;
const collectionName = "Azuki";
const assetName = "Azuki #1313";
const assetPath = "https://ikzttp.mypinata.cloud/ipfs/QmYDvPAXtiJg7s8JdRBSLWdgSphQdac8j1YuQNNxcGE1hg";
const collectionDescription = "Azuki NFT Itms";
const assetThumbnail = "https://ikzttp.mypinata.cloud/ipfs/QmYDvPAXtiJg7s8JdRBSLWdgSphQdac8j1YuQNNxcGE1hg/1313.png";
const assetMetadataobjs = {1: assetName, 2: collectionName, 3: assetPath};
const assetProperties = {1: JSON.stringify([
	{
		"trait_type": "Type",
		"value": "Human"
	},
	{
		"trait_type": "Hair",
		"value": "Powder Blue Swept Back"
	},
	{
		"trait_type": "Clothing",
		"value": "Azuki Track Jacket"
	},
	{
		"trait_type": "Eyes",
		"value": "Pensive"
	},
	{
		"trait_type": "Mouth",
		"value": "Meh"
	},
	{
		"trait_type": "Background",
		"value": "Off White B"
	}
])}
const assetCID = "QmYDvPAXtiJg7s8JdRBSLWdgSphQdac8j1YuQNNxcGE1hg";


// We need to set timeout for a higher number, because some transactions might take up some time
jest.setTimeout(50000);

describe("Nft Items", () => {
	// Instantiate emulator and path to Cadence files
	beforeEach(async () => {
		const basePath = path.resolve(__dirname, "../../");
		const port = 7002;
		await init(basePath, { port });
		await emulator.start(port, false);
		return await new Promise(r => setTimeout(r, 1000));
	});

	// // Stop emulator, so it could be restarted
	afterEach(async () => {
		await emulator.stop();
		return await new Promise(r => setTimeout(r, 1000));
	});

	it("should deploy NftItems contract", async () => {
		await shallPass(deployNftItems());
	});

	it("supply should be 0 after contract is deployed", async () => {
		// Setup
		await deployNftItems();
		const NftAdmin = await getNftAdminAddress();
		await shallPass(setupNftItemsOnAccount(NftAdmin));

		const [supply] = await shallResolve(getNftItemSupply())
		expect(supply).toBe(0);
	});

	it("should be able to mint a Nft item", async () => {
		// Setup
		await deployNftItems();
		const Alice = await getAccountAddress("Alice");
		await setupNftItemsOnAccount(Alice);

		// Mint instruction for Alice account shall be resolved
		await shallPass(mintNftItem(Alice, collectionName, collectionDescription, assetThumbnail, assetMetadataobjs, assetProperties, assetCID, assetPath));
	});

	it("should be able to create a new empty NFT Collection", async () => {
		// Setup
		await deployNftItems();
		const Alice = await getAccountAddress("Alice");
		await setupNftItemsOnAccount(Alice);

		// shall be able te read Alice collection and ensure it's empty
		const [itemCount] = await shallResolve(getNftItemCount(Alice))
		expect(itemCount).toBe(0);
	});

	it("should not be able to withdraw an NFT that doesn't exist in a collection", async () => {
		// Setup
		await deployNftItems();
		const Alice = await getAccountAddress("Alice");
		const Bob = await getAccountAddress("Bob");
		await setupNftItemsOnAccount(Alice);
		await setupNftItemsOnAccount(Bob);

		// Transfer transaction shall fail for non-existent item
		await shallRevert(transferNftItem(Alice, Bob, 1337));
	});

	it("should be able to withdraw an NFT and deposit to another accounts collection", async () => {
		await deployNftItems();
		const Alice = await getAccountAddress("Alice");
		const Bob = await getAccountAddress("Bob");
		await setupNftItemsOnAccount(Alice);
		await setupNftItemsOnAccount(Bob);

		// Mint instruction for Alice account shall be resolved
		await shallPass(mintNftItem(Alice, collectionName, collectionDescription, assetThumbnail, assetMetadataobjs, assetProperties, assetCID, assetPath));

		// Transfer transaction shall pass
		await shallPass(transferNftItem(Alice, Bob, 0));
	});
});
