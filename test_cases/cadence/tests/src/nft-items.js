import { mintFlow, executeScript, sendTransaction, deployContractByName } from "flow-js-testing";
import { getNftAdminAddress } from "./common";

/*
 * Deploys NonFungibleToken and NftItems contracts to NftAdmin.
 * @throws Will throw an error if transaction is reverted.
 * @returns {Promise<[{*} txResult, {error} error]>}
 * */
export const deployNftItems = async () => {
	const NftAdmin = await getNftAdminAddress();
	await mintFlow(NftAdmin, "10.0");

	await deployContractByName({ to: NftAdmin, name: "FungibleToken" });
	await deployContractByName({ to: NftAdmin, name: "NonFungibleToken" });
	await deployContractByName({ to: NftAdmin, name: "MetadataViews" });
	return deployContractByName({ to: NftAdmin, name: "NftItems" });
};

/*
 * Setups NftItems collection on account and exposes public capability.
 * @param {string} account - account address
 * @returns {Promise<[{*} txResult, {error} error]>}
 * */
export const setupNftItemsOnAccount = async (account) => {
	const name = "NftItems/setup_account";
	const signers = [account];

	return sendTransaction({ name, signers });
};

/*
 * Returns NftItems supply.
 * @throws Will throw an error if execution will be halted
 * @returns {UInt64} - number of NFT minted so far
 * */
export const getNftItemSupply = async () => {
	const name = "NftItems/get_Nft_items_supply";

	return executeScript({ name });
};

/*
 * Mints NftItem of a specific **itemType** and sends it to **recipient**.
 * @param {UInt64} itemType - type of NFT to mint
 * @param {string} recipient - recipient account address
 * @returns {Promise<[{*} result, {error} error]>}
 * */
export const mintNftItem = async (recipient, collectionName, collectionDescription, assetThumbnail, assetMetadataobjs, assetProperties, assetCID, assetPath) => {
	const NftAdmin = await getNftAdminAddress();

	const name = "NftItems/mint_Nft_item";
	const args = [recipient, collectionName, collectionDescription, assetThumbnail, assetMetadataobjs, assetProperties, assetCID, assetPath];
	const signers = [NftAdmin];
	return sendTransaction({ name, args, signers });
};

/*
 * Transfers NftItem NFT with id equal **itemId** from **sender** account to **recipient**.
 * @param {string} sender - sender address
 * @param {string} recipient - recipient address
 * @param {UInt64} itemId - id of the item to transfer
 * @throws Will throw an error if execution will be halted
 * @returns {Promise<*>}
 * */
export const transferNftItem = async (sender, recipient, itemId) => {
	const name = "NftItems/transfer_Nft_item";
	const args = [recipient, itemId];
	const signers = [sender];

	return sendTransaction({ name, args, signers });
};

/*
 * Returns the NftItem NFT with the provided **id** from an account collection.
 * @param {string} account - account address
 * @param {UInt64} itemID - NFT id
 * @throws Will throw an error if execution will be halted
 * @returns {UInt64}
 * */
export const getNftItem = async (account, itemID) => {
	const name = "NftItems/get_Nft_item";
	const args = [account, itemID];

	return executeScript({ name, args });
};

/*
 * Returns the number of Nft Items in an account's collection.
 * @param {string} account - account address
 * @throws Will throw an error if execution will be halted
 * @returns {UInt64}
 * */
export const getNftItemCount = async (account) => {
	const name = "NftItems/get_collection_length";
	const args = [account];

	return executeScript({ name, args });
};
