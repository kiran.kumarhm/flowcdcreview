import NonFungibleToken from "../../contracts/NonFungibleToken.cdc"
import NftItems from "../../contracts/NftItems.cdc"

// This script returns the size of an account's NftItems collection.

pub fun main(address: Address): Int {
    let account = getAccount(address)

    let collectionRef = account.getCapability(NftItems.CollectionPublicPath)!
        .borrow<&{NonFungibleToken.CollectionPublic}>()
        ?? panic("Could not borrow capability from public collection")
    
    return collectionRef.getIDs().length
}
