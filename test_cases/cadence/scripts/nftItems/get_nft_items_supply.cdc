import NftItems from "../../contracts/NftItems.cdc"

// This scripts returns the number of NftItems currently in existence.

pub fun main(): UInt64 {    
    return NftItems.totalSupply
}
