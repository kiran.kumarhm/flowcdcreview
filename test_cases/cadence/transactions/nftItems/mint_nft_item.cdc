import NonFungibleToken from 0xNONFUNGIBLETOKEN
import NftItems from 0xNFTITEMS

// This script uses the NFTMinter resource to mint a new NFT
// It must be run with the account that has the minter resource
// stored in /storage/NFTMinter

transaction(
    recipient: Address,
    name: String,
    description: String,
    thumbnail: String,
    metadataobjs: {UInt64: { String : String }},
    properties: {UInt64: { String : String }},
    cid: String, 
    path: String,
 ) {

    // local variable for storing the minter reference
    let minter: &NftItems.NFTMinter

    prepare(signer: AuthAccount) {
        // borrow a reference to the NFTMinter resource in storage
        self.minter = signer.borrow<&NftItems.NFTMinter>(from: NftItems.MinterStoragePath)
            ?? panic("Could not borrow a reference to the NFT minter")
    }

    execute {
        // Borrow the recipient's public NFT collection reference
        let receiver = getAccount(recipient)
            .getCapability(NftItems.CollectionPublicPath)
            .borrow<&{NonFungibleToken.CollectionPublic}>()
            ?? panic("Could not get receiver reference to the NFT Collection")

        // Mint the NFT and deposit it to the recipient's collection
        self.minter.mintNFT(
            recipient: receiver,
            name: name,
            description: description,
            thumbnail: thumbnail,
            metadataobjs:metadataobjs,
            properties:properties,
            cid:cid,
            path:path,
        )

        log("Minted an NFT")
    }
}