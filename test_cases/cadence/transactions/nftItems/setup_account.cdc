import NonFungibleToken from 0xNONFUNGIBLETOKEN
import NftItems from 0xNFTITEMS
import MetadataViews from 0xMETADATAPLACEHOLDER

// This transaction configures an account to hold NFT Items.

transaction {

    prepare(signer: AuthAccount) {
        // Return early if the account already has a collection
        if signer.borrow<&NftItems.Collection>(from: NftItems.CollectionStoragePath) != nil {
            return
        }

        // Create a new empty collection
        let collection <- NftItems.createEmptyCollection()

        // save it to the account
        signer.save(<-collection, to: NftItems.CollectionStoragePath)

        // create a public capability for the collection
        signer.link<&{NonFungibleToken.CollectionPublic, MetadataViews.ResolverCollection}>(
            NftItems.CollectionPublicPath,
            target: NftItems.CollectionStoragePath
        )
    }

    execute {
      log("Setup account")
    }
}